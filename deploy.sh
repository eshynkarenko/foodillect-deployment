#!bin/bash

docker --version
if [ $? -eq 0 ]; then
	echo "Docker is already installed"
else
	sudo apt-get install -y docker.io
fi

git --version
if [ $? -eq 0 ]; then
	echo "Git is already installed"
else
	sudo apt-get install -y git
fi

if [[ -d foodillect ]]; then
	cd foodillect
	git pull
else
	git clone https://eshynkarenko@bitbucket.org/temaovech/foodillect.git
	cd foodillect
fi

docker build -t $HEROKU_APP_NAME --build-arg BOT_TOKEN=$BOT_TOKEN --build-arg CHAT_ID=$CHAT_ID --build-arg API_KEY=$API_KEY --build-arg REDIS_USER=$REDIS_USER --build-arg REDIS_PASSWORD=$REDIS_PASSWORD --build-arg REDIS_HOST=$REDIS_HOST --build-arg REDIS_PORT=$REDIS_PORT .

docker login --username=_ --password=$HEROKU_AUTH_TOKEN registry.heroku.com

docker tag $HEROKU_APP_NAME registry.heroku.com/$HEROKU_APP_NAME/service

docker push registry.heroku.com/$HEROKU_APP_NAME/service

WEB_DOCKER_IMAGE_ID="$(docker inspect registry.heroku.com/$HEROKU_APP_NAME/service --format={{.Id}})"

request_data()
{
	cat <<EOF
{
        "updates": [{
		"type": "service",
		"docker_image": "$WEB_DOCKER_IMAGE_ID"
	}]
}
EOF
}

curl -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP_NAME/formation \
	-d "$(request_data)" \
	-H "Content-Type: application/json" \
	-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
	-H "Authorization: Bearer $HEROKU_AUTH_TOKEN"