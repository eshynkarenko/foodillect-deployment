import subprocess
from flask import Flask, request

app = Flask(__name__)


@app.route('/deploy', methods=['POST'])
def deploy():
    if request.method == 'POST':
        request_json = request.get_json(force=True)
        branch_name = request_json["push"]["changes"][0]["new"]["name"]
        print(branch_name)
        if branch_name == "master":
            proc = subprocess.Popen("bash deploy.sh", shell=True, stdout=subprocess.PIPE)
            print(proc.stdout.read())
    return '', 204


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
